<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 12</title>
  </head>
  <body>
    <h1>Ejercicio 1</h1>
    <h3>Ejercicio 1.1</h3>
    <p>El strict_types=1, sirve para especificar el valor tipo de dato que se debe introducir <br> en los parametros de una funcion, si queremos que sea entero lo podemos especificar por ejemplo:<br><br>declare(strict_types=1);<br>
function suma(int $a, int $b) {<br>
    return $a + $b;<br>
}<br><br>var_dump(suma(7, 8));<br>
var_dump(suma(1.5, 2.5));<br> <br>El resultado nos daría 3 debido a la primera linéa y un herror por la segunda, debido a que introducimos variables float y no int<br>La siguiente linea es el resultado</p>

  <?php
  function suma(int $a, int $b) {
      return $a + $b;
  }

  var_dump(suma(7, 8));
  //var_dump(suma(1.5, 2.5)); esta linea es con otro tipo de dato y genera un error
  ?>
  <p>Fatal error: Uncaught TypeError: Argument 1 passed to suma() must be of the type int, float given, called in /var/www/html/practicas/A_12/A12.php on line 23 and defined in /var/www/html/practicas/A_12/A12.php:18 Stack trace: #0 /var/www/html/practicas/A_12/A12.php(23): suma(1.5, 2.5) #1 {main} thrown in /var/www/html/practicas/A_12/A12.php on line 18</p>
  <h3>Ejercicio 1.2 y 1.3</h3><br>
  <p>Para que nuestra funcion solo devuelva un tipo de valor debemos haverlo asi <br><br>function strings(string $nombre ): string
  <br><br><br>Para que nos devuelva un tipo de valor o null asi<br><br> function strings(string $nombre ): ?string <br><br> Este es el ejemplo:</p>
  <?php
  function strings(string $nombre ): ?string{
    if ($nombre!="roger") {
      $hola="hola ".$nombre;
      return $hola;
    }else {
      return null;
    }
  }
  $pruebas = strings("roger");
  var_dump($pruebas);
  $prueba = strings("Javier");
  var_dump($prueba);
    ?>
<br><h1>Ejercicio 2</h1>

  <?php
  function insert($nomTabla, $datos){

    $sentencia = "insert into ".$nomTabla." (".implode(",",array_keys($datos)).") values "." (:".implode(", :",$datos).")";

    return $sentencia;
  }
  $persona=["ID"=>"01","nombre"=>"Roger","apellido"=>"Berrio Mira", "edad"=>18 , "Nacionalidad"=>"Colombiana"];
  $tabla="personas";
  $sql = insert($tabla,$persona);
  var_dump($sql);
    ?>

    <h1>Ejercicio 3</h1><br>
    <?php
    function insert2($nomTabla, $datos, &$cadena){

      $cadena = "insert into ".$nomTabla." (".implode(",",array_keys($datos)).") values "." (:".implode(", :",$datos).")";


    }
    $insersion="insert into tabla (campos) values (valores)";
    insert2($tabla,$persona,$insersion);
    var_dump($insersion);
      ?>
  </body>
</html>
